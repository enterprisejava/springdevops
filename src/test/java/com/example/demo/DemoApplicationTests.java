package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void homeResponse() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("Spring is not here!");
	}

	@Test
	public void aboutResponse() {
		String body = this.restTemplate.getForObject("/about/me", String.class);
		assertThat(body).isEqualTo("me");
	}

	@Test
	public void aboutYouResponse() {
		String body = this.restTemplate.getForObject("/about/you", String.class);
		assertThat(body).isEqualTo("you");
	}

	@Test
	public void greetingDefaultResponse() {
		String body = this.restTemplate.getForObject("/greeting", String.class);
		System.out.println(body);
        assertThat(body).isEqualTo("{\"id\":1,\"content\":\"Hello, World!\"}");
	}
}
